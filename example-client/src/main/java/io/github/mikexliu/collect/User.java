package io.github.mikexliu.collect;

public class User {

    public String name;
    public Integer age;

    public void growUp() {
        age++;
    }
}
